#!/usr/bin/env python
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN


tourDescription = """

# TDD Controlled RF

Use this profile to deploy an end-to-end LTE network in a controlled RF
environment (SDRs with wired connections) using using OAI (eNB and CN) and
srsLTE (UE). The RAN is configured to use TDD on band 41. An orchestration node
is deployed as well for testing network throughput. This profile is largely
based on
[this](https://github.com/OPENAIRINTERFACE/openair-cn/wiki/Full-Deployment-of-OAI-system)
tutorial.

The following nodes will be deployed:

* Intel NUC5300/B210 w/ srsLTE (`rue1`)
* Intel NUC5300/B210 w/ OAI RAN (`enb1`)
* D430 compute node w/ OAI CN (`epc`)
* generic compute node for orchestrating tests (`orch`)

"""

tourInstructions = """

### Preliminary setup after the experiment becomes ready

After the nodes boot for the first time, ssh into `rue1`, `enb1`, and `epc` and
do the following:

```
/local/repository/bin/setup.sh
```

This will install the necessary software and configuration files on each node.
It may take several minutes for the `enb1` and `epc` nodes. The script will exit
on error; make sure it completes on all three nodes before moving on. These
changes will persist after rebooting the nodes, so you only need to do this the
first time the nodes boot.

### Starting the end-to-end network

After the preliminary setup has completed without error, do the following, first
on `epc`, next on `enb1`, and finally on `rue1` (the order is important here):

```
/local/repository/bin/start.sh
```

Note that, on `epc`, this will start a `tmux` session with four panes,
running HSS, MME, SPGW-C, and SPGW-U. If you are not familiar with `tmux`,
it's a terminal multiplexer that has some similarities to screen. Here's a [tmux
cheat sheet](https://tmuxcheatsheet.com), but `ctrl-b o` (move to other
pane) and `ctrl-b x` (kill pane), should get you pretty far. If you'd rather
start the `epc` processes another way, here are the commands (order is important
here as well):

```
# in separate consoles, unless you're backrgounding the processes

# start HSS
cd /local/openair-cn/scripts
oai_hss -j /usr/local/etc/oai/hss_rel14.json

# start MME
cd /local/openair-cn/scripts
./run_mme --config-file /usr/local/etc/oai/mme.conf --set-virt-if

# start SPGW-C
sudo spgwc -c /usr/local/etc/oai/spgw_c.conf

# start SPGW-U
sudo spgwu -c /usr/local/etc/oai/spgw_u.conf
```

### Testing
You can use the orchestration node `orch` to test as follows:

#### Ping the UE
on `orch`

```
ping -c 20 12.1.1.2
```
#### Iperf DL UDP
on `rue1`

```
iperf -B 12.1.1.2 -u -s -i 1 -fm
```

on `orch`

```
iperf -c 12.1.1.2 -u -b 2.00M -t 30 -i 1 -fm
```

#### Iperf UL UDP
on `orch`

```
iperf -u -s -i 1 -p 5001 -fm
```

on `rue1`

```
ORCHIP=$(getent hosts orch | awk '{ print $1 }')
iperf -c $ORCHIP -u -b 2.00M -t 30 -i 1 -fm  -p 5001 -B 12.1.1.2
```
"""


class GLOBALS(object):
    UBUNTU_1804_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    UBUNTU_1804_LOW_LATENCY_IMG = "urn:publicid:IDN+emulab.net+image+PowderProfiles:ubuntu1804lowlatency:1"
    NUC_HWTYPE = "nuc5300"


pc = portal.Context()
pc.bindParameters()
pc.verifyParameters()

request = pc.makeRequestRSpec()

# Add a NUC eNB node
enb1 = request.RawPC("enb1")
enb1.hardware_type = GLOBALS.NUC_HWTYPE
enb1.disk_image = GLOBALS.UBUNTU_1804_LOW_LATENCY_IMG
enb1.Desire("rf-controlled", 1)
enb1_rue1_rf = enb1.addInterface("rue1_rf")

# Add a NUC UE node
rue1 = request.RawPC("rue1")
rue1.hardware_type = GLOBALS.NUC_HWTYPE
rue1.disk_image = GLOBALS.UBUNTU_1804_LOW_LATENCY_IMG
rue1.Desire("rf-controlled", 1)
rue1_enb1_rf = rue1.addInterface("enb1_rf")

# Create the RF link between the UE and eNodeB
rflink = request.RFLink("rflink")
rflink.addInterface(enb1_rue1_rf)
rflink.addInterface(rue1_enb1_rf)

# Add OAI EPC (HSS, MME, SPGW) node.
epc = request.RawPC("epc")
epc.hardware_type = "d430"
epc.disk_image = GLOBALS.UBUNTU_1804_IMG
epc.addService(rspec.Execute(shell="bash", command="/local/repository/bin/setup-epc-network.sh"))

# Add an orchestrator node for testing
orch = request.RawPC("orch")
orch.disk_image = GLOBALS.UBUNTU_1804_IMG
orch.addService(rspec.Execute(shell="bash", command="/local/repository/bin/setup-orch-network.sh"))

link = request.Link("lan")
link.addNode(rue1)
link.addNode(enb1)
link.addNode(epc)
link.addNode(orch)
link.link_multiplexing = True
link.vlan_tagging = True
link.best_effort = True

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
