#!/usr/bin/env bash

# Run appropriate setup script

NODE_ID=$(geni-get client_id)

if [ $NODE_ID = "rue1" ]; then
    /local/repository/bin/setup-ue.sh
elif [ $NODE_ID = "enb1" ]; then
    /local/repository/bin/setup-enb.sh
elif [ $NODE_ID = "epc" ]; then
    /local/repository/bin/setup-epc.sh
else
    echo "no setup necessary"
fi
