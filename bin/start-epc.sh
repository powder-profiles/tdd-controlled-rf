#!/bin/bash

# Setup a tmux session with hss/mme/spgw-c/spgw-u

set -ux
tmux has-session -t epc 2>/dev/null
if [ $? == 0 ]; then
    for _pane in $(tmux list-panes -F '#P'); do
        tmux send-keys -t ${_pane} C-c
    done
    tmux kill-session -t epc
fi

tmux new-session -d -s epc
tmux send-keys 'cd /local/openair-cn/scripts; oai_hss -j /usr/local/etc/oai/hss_rel14.json' C-m
sleep 0.5
tmux split-window -v
tmux send-keys 'cd /local/openair-cn/scripts; ./run_mme --config-file /usr/local/etc/oai/mme.conf --set-virt-if' C-m
sleep 0.5
tmux split-window -v
tmux send-keys 'sudo spgwc -c /usr/local/etc/oai/spgw_c.conf' C-m
sleep 0.5
tmux split-window -v
tmux send-keys 'sudo spgwu -c /usr/local/etc/oai/spgw_u.conf' C-m
sleep 0.5
tmux select-layout even-vertical
tmux attach-session -d -t epc
