#!/bin/bash

# setup routes for orchestration node

set -eux
EPCIP=$(getent hosts epc | awk '{ print $1 }')
VLANIF=$(ifconfig | grep vlan | cut -d ":" -f1)

sudo ip route add 12.0.0.0/24 via $EPCIP dev $VLANIF
sudo ip route add 12.1.1.0/24 via $EPCIP dev $VLANIF
